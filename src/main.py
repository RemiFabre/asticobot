import random, math, pygame
import pygame.draw
from pygame.locals import *
import json
import pypot.robot
import time
import sys
import pypot.dynamixel
from contextlib import closing
import logging
import logging.config

# Acceptable pos are [-55, 38]


def init_robot(dxl_io, goal_positions):

    ids = []
    for (id, position) in goal_positions:
        ids.append(id)

    dxl_io.enable_torque(ids)

    print "Going to init position : ", goal_positions
    smoothlyGoToPosition(dxl_io, goal_positions, 1, 100)


# Sending a sin wave to each servomotor with a tunable gain, freq, offset and delay. Units are degrees,  seconds and a percentage for phi (0.2 means 0.2*2*pi secs of delay).
def sinuses(dxl_io, offset, freq, amplitude, phi, ids):
    RAD2DEG = 180.0 / math.pi
    DEG2RAD = math.pi / 180.0
    t0 = time.time()
    while True:
        d0 = time.time()
        id_counter = 0
        list_of_pos = []
        for id in ids:
            id_counter = id_counter + 1
            t = time.time() - t0
            pos = offset + amplitude * math.sin(
                2 * math.pi * freq * t + 2 * math.pi * phi * id_counter
            )
            list_of_pos.append(int(pos))
        try:
            dict_of_pos = dict(zip(ids, list_of_pos))
            dxl_io.set_goal_position(dict_of_pos)
        except:
            print ("Com error")
        time.sleep(0.001)
        d = time.time()
        print ("f = " + str(1.0 / (d - d0)))


def smoothlyGoToPosition(dxlIo, goalPositions, timeToDestination=4, freq=50):
    print "Going to positions : ", goalPositions
    ids = []
    sleepTime = 0.001
    writingPeriod = 1.0 / freq + sleepTime

    for (id, position) in goalPositions:
        ids.append(id)
        time.sleep(sleepTime)

    # Getting positions at the begining
    presentPositions = dxlIo.get_present_position(ids)
    print "presentPositions = ", presentPositions
    time.sleep(sleepTime * len(goalPositions))

    i = 0
    deltas = []
    for (id, position) in goalPositions:
        deltas.append(position - presentPositions[i])
        i = i + 1

    print "Deltas = ", deltas
    elapsedTime = 0
    errorCounter = 0
    while elapsedTime < timeToDestination:
        progress = elapsedTime / timeToDestination
        outputCommands = {}
        # Building output vector with new commands
        i = 0
        for (id, position) in goalPositions:
            outputCommands[id] = int(presentPositions[i] + progress * deltas[i])
            i = i + 1
        print "Vector = ", outputCommands
        try:
            dxlIo.set_goal_position(outputCommands)
        except:
            errorCounter = errorCounter + 1
            print "errorCounter = ", errorCounter

        elapsedTime = elapsedTime + writingPeriod
        print progress * 100, "%"
        time.sleep(writingPeriod)

    return


def special_circle(dxl_io, offset, amplitude, phi, list_of_ids):
    pygame.init()
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode([800, 600])
    screen.fill([0, 0, 0])
    background = pygame.image.load("lasticow-boy.jpg")
    screen.blit(background, [0, 0])
    pygame.display.update()
    stop = False
    circle_angle = -2
    to_the_left = {
        2: int(offset - 2 * amplitude),
        3: int(offset - amplitude),
        4: int(offset),
        5: int(offset + amplitude),
        6: int(offset + 2 * amplitude),
    }
    to_the_right = {
        2: int(offset + 2 * amplitude),
        3: int(offset + amplitude),
        4: int(offset),
        5: int(offset - amplitude),
        6: int(offset - 2 * amplitude),
    }
    circle = {
        2: int(circle_angle),
        3: int(circle_angle),
        4: int(circle_angle),
        5: int(circle_angle),
        6: int(circle_angle),
    }
    while stop == False:
        events = pygame.event.get()
        for e in events:
            if e.type == QUIT:
                sys.exit()
            elif e.type == KEYDOWN:
                key = e.dict["unicode"]
                if key == "a":
                    print "left"
                    dxl_io.set_goal_position(to_the_left)
                elif key == "p":
                    print "right"
                    dxl_io.set_goal_position(to_the_right)
                elif key == " ":
                    print "circle"
                    dxl_io.set_goal_position(circle)

        # erasing before rewritting
        screen.blit(background, [0, 0])
        pygame.display.update()
        clock.tick(60)


if __name__ == "__main__":
    if len(sys.argv) < 5:
        print ("Usage : main.py offset freq amplitude phi [P] [I] [D]")
        print ("if freq == c, the special circle mode is activated")
    else:
        circle = False
        if sys.argv[2] == "c":
            print ("Circle mode activated")
            circle = True
            try:
                offset = float(sys.argv[1])
                amplitude = float(sys.argv[3])
                phi = float(sys.argv[4])
            except:
                None
        else:
            try:
                offset = float(sys.argv[1])
                freq = float(sys.argv[2])
                amplitude = float(sys.argv[3])
                phi = float(sys.argv[4])
            except:
                print ("An error occured, all parameters should be floats")
                sys.exit()

        k_p = 4
        k_i = 0
        k_d = 0
        try:
            # Handling optional parameters (PID)
            k_p = float(sys.argv[5])
            k_i = float(sys.argv[6])
            k_d = float(sys.argv[7])
        except:
            None

        ports = pypot.dynamixel.get_available_ports()
        if not ports:
            raise IOError("No port found!")
        print ("Ports found", ports)
        print ("Connecting on the first available port:", ports[0])
        dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)
        list_of_ids = [2, 3, 4, 5, 6]
        print ("Positions = ", dxl_io.get_present_position(list_of_ids))

        print ("Setting PID values to ", k_p, k_i, k_d)
        repeat_pid = []
        for i in range(len(list_of_ids)):
            repeat_pid.append((k_p, k_i, k_d))
        dict_of_pid = dict(zip(list_of_ids, repeat_pid))
        dxl_io.set_pid_gain(dict_of_pid)

        print ("Robot init")
        start_angle = offset
        goal_positions = [
            (2, start_angle),
            (3, start_angle),
            (4, start_angle),
            (5, start_angle),
            (6, start_angle),
        ]
        init_robot(dxl_io, goal_positions)

        if circle:
            print ("Starting circle")
            special_circle(dxl_io, offset, amplitude, phi, list_of_ids)
        else:
            print ("Starting sinuses")
            sinuses(dxl_io, offset, freq, amplitude, phi, list_of_ids)
