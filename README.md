[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# Video of Asticobot
https://www.youtube.com/watch?v=FdskzVZhmFs

# Ideas of improvement
- Untethered
- Bit its own tail to form a circle. Change the shape of the circle to make it roll (potentially fast !)
- Change direction (motors rotational axis should not all be aligned)
- Automatically dances to the rhythm of a music
